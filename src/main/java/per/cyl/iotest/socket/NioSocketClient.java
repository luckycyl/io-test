package per.cyl.iotest.socket;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Random;

/**
 * @author 陈玉林
 * @desc TODO
 * @date 2020/7/16 15:13
 */
public class NioSocketClient implements Runnable {
    private int i  = -1;
    public NioSocketClient() {
    }

    public NioSocketClient(int i) {
        this.i = i;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {

        for (int i = 0 ; i < 1; i ++) {
            new Thread(new NioSocketClient(i)).start();

        }

    }

    @Override
    public void run() {
        startClient(i);
    }

    public void startClient(int i) {
        SocketChannel socketChannel = null;
        Selector selector = null;

        ByteBuffer writeBuffer = ByteBuffer.allocate(1024);
        ByteBuffer readBuffer = ByteBuffer.allocate(1024);

        try {
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress("127.0.0.1", 8080));

            selector = Selector.open();
            socketChannel.register(selector, SelectionKey.OP_CONNECT);

            if (socketChannel.finishConnect()) {

                while (true) {
                    String info = "客户端-" + new Random().nextInt() ;
                    System.out.println("-------------------------------------------------------------");
                    writeBuffer.clear();
                    writeBuffer.put(info.getBytes("utf-8"));
                    writeBuffer.flip();

                    while (writeBuffer.hasRemaining()) {
                        socketChannel.write(writeBuffer);

                    }

                    int bytesRead = socketChannel.read(readBuffer);
                    if (bytesRead > 0) {
                        readBuffer.flip();
                        byte[] bytes = new byte[bytesRead];
                        readBuffer.get(bytes, 0, bytesRead);
                        String str = new String(bytes);
                        System.out.println(str);
                        readBuffer.clear();

                    }
                    Thread.sleep(2000);
//                    Thread.sleep(Integer.MAX_VALUE);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (socketChannel != null) {
                    socketChannel.close();
                    System.out.println("关闭socketChannel");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void hook () {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {}
        }));
    }
}
